﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test01
{
    class NestedLoop
    {
        public NestedLoop() { }
        public void Loop01(int input)
        {
            if (input > 0)
            {
                if (input > 10)
                {
                    if (input < 9)
                    {
                        Console.WriteLine($"This is a fucking unused code");
                    }
                    else
                    {
                        if (input % 2 == 0)
                        {
                            Console.WriteLine($"{input} is > 10 and {input} is an Even");
                        }
                        else
                        {
                            Console.WriteLine($"{input} is > 10 and {input} is an Odd");
                        }
                    }
                }
                else
                {
                    if (input % 2 == 0)
                    {
                        Console.WriteLine($"{input} is > 0 and {input} < 10 and {input} is an Even");
                    }
                    else
                    {
                        Console.WriteLine($"{input} is > 0 and {input} < 10 and {input} is an Odd");
                    }
                }
            }
            else if (input < 0)
            {
                if (input < -10)
                {
                    if (input % 2 == 0)
                    {
                        Console.WriteLine($"{input} is < -10 and {input} is an Even");
                    }
                    else
                    {
                        Console.WriteLine($"{input} is < -10 and {input} is an Odd");
                    }
                }
                else
                {
                    if (input % 2 == 0)
                    {
                        Console.WriteLine($"{input} is < 0 and {input} > -10 and {input} is an Even");
                    }
                    else
                    {
                        Console.WriteLine($"{input} is < 0 and {input} > -10 and {input} is an Odd");
                    }
                }
            }
            else
            {
                Console.WriteLine($"{input} = 0");
            }
        }
    }
}
